const express = require('express');
const db = require('./database/db');

//api paths
const walletLock= require('./api/lockWallet');
const newAddr 	= require('./api/newAddr');
const signTx 	= require('./api/signTx');
const privKey 	= require('./api/privKey');

const PORT = 8081;
const HOST = '0.0.0.0';

// App
const app = module.exports = express();

// Grabs the json on post
app.use(express.urlencoded({extended: true}));
app.use(express.json());

// Use the api routes
app.use('/wallet/lock', walletLock);
app.use('/wallet/address', newAddr);
app.use('/wallet/transaction', signTx);
app.use('/wallet', privKey); //Returns the private key associated with a given address

// all the global variables
global.g_unlock_timeout = 0; //initialize timeout
global.g_mnemonic = ""; //initialize menmonic
global.g_locked = false;
global.DBK = {
    LOCKED : "locked",
    MNEMONIC : "mnemonic",
    LASTNONCE : "last_nonce"
};
var initial_nonce = 10000;

db.put(DBK.LOCKED, false, function (err) {
    if (err) {
        console.log("err: " + err);
        return err;
    }
});

db.get(DBK.LASTNONCE, function (err, value) {
    if (err) {
        // last nonce not in the db
        db.put(DBK.LASTNONCE, initial_nonce, function (err) {
            if (err) {
                console.log('err: '+ err); // likely db file corrupted
                return err;
            }
        });
    }
});

app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);
