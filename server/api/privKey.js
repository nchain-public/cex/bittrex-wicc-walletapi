const express = require('express');
const router = express.Router();
const db = require('../database/db');
// grab the package.json from SDK directory
const bitcore = require('../../wicc-wallet-utils-js/.');
// aes js to encrypt
var aesjs = require('aes-js');
const utils = require('../utils');
// choose the network type.
var arg = {network: 'mainnet'};
var wiccApi = new bitcore.WiccApi(arg);

// @route         GET wallet/:addr
// @description   gets the private key from the given address
// @access        Public

router.get('/:addr', (request, response) => {

    let address = request.params.addr;

    if (g_locked === false && g_unlock_timeout > 0 && g_unlock_timeout > Date.now()){

        db.get(DBK.LOCKED, function (err, lockedStr) {
            if (err) {
                console.log("err: " + err);
                return err;
            }

            let is_locked = (lockedStr == "true");

            if (is_locked) {
                console.log("wallet is locked");
                g_mnemonic = "";
                response.status(403).end();
                return;
            }

            //check if unlock is time
            if (g_unlock_timeout > 0 && g_unlock_timeout < Date.now()) {
                db.put(DBK.LOCKED, true, function (err) {
                    if (err) {
                        console.log("err: " + err);
                        return err;
                    }

                    g_locked = true;
                });

                g_mnemonic = "";
                response.status(403).send("access denied since unlock timed out!");
                return;
            }

            db.get("ADDR_" + address, function (err, addrNonceValue) {
                if (err) {
                    console.log("Err: " + err);
                    return err;
                }

                addrNonceValue = parseInt(addrNonceValue);
                console.log("addrNonceValue: " + addrNonceValue);
                console.log("address: " + address);

                // get private key from mnemonic code
                // console.log("g_mnemonic: [" + g_mnemonic + "]");

                var privateKey = wiccApi.getPriKeyFromMnemonicCode(g_mnemonic, addrNonceValue);

                response.json({"key": privateKey});
                console.log(privateKey)

            });
        })// end of put mnemonic code
    }else{
        console.log("the wallet is locked, unlock it first");
        response.json("the wallet is locked, unlock it first")
    }
 });

module.exports = router;
