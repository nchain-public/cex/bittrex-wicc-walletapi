var aesjs = require('aes-js');
var pbkdf2 = require('pbkdf2'); // allows you to use a normal password but then converts to easy to use array

var AesEncrypt = function (srcText, password) {
    var key_128 = pbkdf2.pbkdf2Sync(password, 'salt', 1, 128 / 8, 'sha512');

    var srcBytes = aesjs.utils.utf8.toBytes(srcText);
    var aesCtr = new aesjs.ModeOfOperation.ctr(key_128, new aesjs.Counter(5));
    var encryptedBytes = aesCtr.encrypt(srcBytes);
    var encryptedHex = aesjs.utils.hex.fromBytes(encryptedBytes);
    console.log("encrypted:" + encryptedHex);

    return encryptedHex;
};

var AesDecrypt = function (srcText, password) {
	var key_128 = pbkdf2.pbkdf2Sync(password, 'salt', 1, 128 / 8, 'sha512');

    var encryptedBytes = aesjs.utils.hex.toBytes(srcText);
    var aesCtr = new aesjs.ModeOfOperation.ctr(key_128, new aesjs.Counter(5));
    var decryptedBytes = aesCtr.decrypt(encryptedBytes);
    // Convert our bytes back into text
    var decryptedText = aesjs.utils.utf8.fromBytes(decryptedBytes);
    console.log(decryptedText);

    return decryptedText;
}

exports.AesEncrypt = AesEncrypt;
exports.AesDecrypt = AesDecrypt;
